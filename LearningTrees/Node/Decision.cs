﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearningTrees
{
    class Decision : INode
    {
        private IAttribute _attribute;
        private float _splitValue;
        private INode _preChild;
        private INode _postChild;
        private float _nullValue;
        private INode _nullChild = null;
        private Dictionary<float, INode> _childs;


        // Constructor
        public Decision(IAttribute attribute)
        {
            _childs = new Dictionary<float, INode>();
            _nullValue = -9;
            _attribute = attribute;
        }


        public void AddNullBranch(INode child)
        {
            _nullChild = child;
        }

        public void AddPreBranch(float value, INode child)
        {
            _splitValue = value;
            _preChild = child;
        }

        public void AddPostBranch(INode child)
        {
            _postChild = child;
        }

        public void AddBranch(float value, INode child)
        {
            _childs.Add(value, child);
        }


        public float CheckClassification(int example)
        {
            float exampleValue = Parser.TESTDATA[example, _attribute.Index];

            if (_attribute is Numerical)
            {
                if (exampleValue == _nullValue)
                {
                    return _nullChild.CheckClassification(example);
                }
                else
                {
                    if (exampleValue > _splitValue)
                        return _postChild.CheckClassification(example);
                    else
                        return _preChild.CheckClassification(example);
                }
            }
            else
                return _childs[exampleValue].CheckClassification(example);
        }
      

        public override string ToString()
        {
            return ToString("\t\t");
        }

        public string ToString(string indent)
        {
            string str = "("+ _attribute.Name + ")\n";

            if(_attribute is Numerical)
            {
                str += indent + "--?--" + _nullChild.ToString(indent+"|\t\t");
                str += indent+ "\\-- <" + _splitValue + "-- " + _preChild.ToString(indent+"|\t\t");
                str += indent + "\\-- >" + _splitValue + "-- " + _postChild.ToString(indent + "\t\t");
            }
            else
            {
                int count = 0;
                foreach (KeyValuePair<float, INode> child in _childs)
                {
                    count++;
                    if(count != _childs.Count)
                    {
                        if (child.Key == -9)
                            str += indent + "\\--?-- " + child.Value.ToString(indent + "|\t\t");
                        else
                            str += indent + "\\--" + child.Key + "-- " + child.Value.ToString(indent + "|\t\t");
                    }
                    else
                    {
                        if (child.Key == -9)
                            str += indent + "\\--?-- " + child.Value.ToString(indent + "\t\t");
                        else
                            str += indent + "\\--" + child.Key + "-- " + child.Value.ToString(indent + "\t\t");
                    }

                }
            }

            return str;
        }
        
    }
}
