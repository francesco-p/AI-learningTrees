﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearningTrees
{
    class Program
    {
        static void Main(string[] args)
        {
            Parser parser;
            List<IAttribute> attributes;
            float correctPredictions = 0;
            float currAccuracy = 0;
            List<int> training = null;
            List<int> test = null;
            LearningTree alg = null;
            INode tree = null;
            int maxDepth = 0;
            List<float> foldAccuracy;
            List<float> depthAvgAccuracy = new List<float>();


            /*******************************************************************************************
            ************************10-FOLD XVALIDATION FOR 23 DIFFERENT DEPTHS*************************
            ********************************************************************************************/
            for (int depth = 23; depth > 0; depth--)
            {
                foldAccuracy = new List<float>();

                // Dependency effect: parser must be created before attributes
                parser = new Parser("Datasets/crossvalidationset.data");


                while (parser.NextSplit())
                {

                    attributes = new List<IAttribute>()
                               {
                                   new Numerical("Age",0),
                                   new Discrete ("Sex",1, new List<float> {-9,0,1}),
                                   new Discrete ("Chest Pain",2, new List<float> { -9,1,2,3,4 } ),
                                   new Numerical ("trestBPS", 3),
                                   new Numerical ("Chol",4),
                                   new Discrete ("FBS", 5, new List<float> { -9, 0,1} ),
                                   new Discrete ("restECG", 6, new List<float> { -9, 0,1,2 } ),
                                   new Numerical("Talalch",7),
                                   new Discrete ("Exang", 8, new List<float> { -9, 0,1}),
                                   new Numerical ("Oldpeak",9),
                                   new Discrete ("Slope", 10, new List<float> { -9, 0,1,2,3} ),
                                   new Discrete ("Ca", 11, new List<float> { -9, 0, 1, 2, 3} ),
                                   new Discrete ("Thal", 12, new List<float> { -9, 3,6,7} )
                               };

                    training = Enumerable.Range(0, Parser.TRAININGDATA.GetLength(0)).ToList();
                    alg = new LearningTree(Parser.TRAININGDATA, depth);
                    tree = alg.DecisionTreeLearning(training, attributes, new Leaf(1), 0);

                    test = Enumerable.Range(0, Parser.TESTDATA.GetLength(0)).ToList();
                    foreach (int i in test)
                        correctPredictions += tree.CheckClassification(i);

                    currAccuracy = correctPredictions / Parser.TESTDATA.GetLength(0);
                    Console.WriteLine("Accuracy : {0}/{1} = {2} @ depth {3} ", correctPredictions, Parser.TESTDATA.GetLength(0), currAccuracy, depth);
                    foldAccuracy.Add(currAccuracy);

                    // Remove refs for GC
                    correctPredictions = 0;
                    training = null;
                    test = null;
                    alg = null;
                    tree = null;
                    attributes = null;
                }

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Average Accuracy: {0} @ depth {1}", foldAccuracy.Average(), depth);
                Console.ResetColor();

                depthAvgAccuracy.Add(foldAccuracy.Average());
                foldAccuracy = null;

                parser = null;
            }


            
            /*******************************************************************************************
            **************************************TRAINING+TEST*****************************************
            ********************************************************************************************/

            Console.ForegroundColor = ConsoleColor.Green;

            maxDepth = 23 - depthAvgAccuracy.IndexOf(depthAvgAccuracy.Max());

            parser = new Parser();
            parser.setTrainingData("Datasets/crossvalidationset.data");

            attributes = new List<IAttribute>()
                    {
                        new Numerical("Age",0),
                        new Discrete ("Sex",1, new List<float> {-9,0,1}),
                        new Discrete ("Chest Pain",2, new List<float> { -9,1,2,3,4 } ),
                        new Numerical ("trestBPS", 3),
                        new Numerical ("Chol",4),
                        new Discrete ("FBS", 5, new List<float> { -9, 0,1} ),
                        new Discrete ("restECG", 6, new List<float> { -9, 0,1,2 } ),
                        new Numerical("Talalch",7),
                        new Discrete ("Exang", 8, new List<float> { -9, 0,1}),
                        new Numerical ("Oldpeak",9),
                        new Discrete ("Slope", 10, new List<float> { -9, 0,1,2,3} ),
                        new Discrete ("Ca", 11, new List<float> { -9, 0, 1, 2, 3} ),
                        new Discrete ("Thal", 12, new List<float> { -9, 3,6,7} )
                    };

            training = Enumerable.Range(0, Parser.TRAININGDATA.GetLength(0)).ToList();
            alg = new LearningTree(Parser.TRAININGDATA, maxDepth);
            tree = alg.DecisionTreeLearning(training, attributes, new Leaf(1), 0);
            Console.WriteLine(tree);

            parser.setTestData("Datasets/testset.data");
            test = Enumerable.Range(0, Parser.TESTDATA.GetLength(0)).ToList();

            foreach (int i in test)
                correctPredictions += tree.CheckClassification(i);

            currAccuracy = correctPredictions / Parser.TESTDATA.GetLength(0);

            Console.WriteLine("Accuracy : {0}/{1} = {2} @ depth {3} ", correctPredictions, Parser.TESTDATA.GetLength(0), currAccuracy, maxDepth);
            Console.ResetColor();

            Console.ReadLine();

        }



    }
}
