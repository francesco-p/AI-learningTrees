﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearningTrees
{
    class LearningTree
    {
        private int classificationIdx;
        private int _maxDepth;

        public LearningTree(float[,] d, int m)
        {
            classificationIdx = Parser.TRAININGDATA.GetLength(1) - 1;
            _maxDepth = m;
        }
       
        public INode DecisionTreeLearning(List<int> examples, List<IAttribute> attributes, INode def, int depth)
        {        
            if (examples.Count == 0)
                return def;

            if (depth == _maxDepth)
                return majorityValue(examples);

            if (AllSameClassification(examples))
                if (Parser.TRAININGDATA[examples[0], classificationIdx] == 1)
                    return new Leaf(1);
                else
                    return new Leaf(0);

            bool allOff = true;
            foreach (IAttribute a in attributes)
            {
                if (a.IsOn)
                {
                    allOff = false;
                    break;
                }
            }
            if (allOff)
                return majorityValue(examples);

            IAttribute best = chooseAttribute(examples, attributes);

            Decision tree = new Decision(best);

            INode m = majorityValue(examples);

            if(best is Discrete)
            {
                best.IsOn = false;

                foreach (float value in ((Discrete)best).Values)
                {
                    List<int> examples1 = CloneList(((Discrete)best).ExamplesOf(value));
                    INode subtree = DecisionTreeLearning(examples1, attributes, m, depth+1);
                    tree.AddBranch(value, subtree);
                }
            }
            else
            {
                List<int> nullList = nullList = ((Numerical)best).NullValueList;
                float valueSplit = valueSplit = ((Numerical)best).Splitvalue;
                List<int> preSplit = preSplit = CloneList(((Numerical)best).PreList);
                List<int> postSplit = postSplit = CloneList(((Numerical)best).PostList);

                INode subtree = DecisionTreeLearning(nullList, attributes, m, depth + 1);
                tree.AddNullBranch(subtree);

                INode subtree2 = DecisionTreeLearning(preSplit, attributes, m, depth + 1);
                tree.AddPreBranch(valueSplit, subtree2);

                INode subtree3 = DecisionTreeLearning(postSplit, attributes, m, depth + 1);
                tree.AddPostBranch(subtree3);
            }
            return tree;
        }

        private bool AllSameClassification(List<int> examples)
        {
            float prev = Parser.TRAININGDATA[examples[0], classificationIdx];

            foreach (int i in examples)
                if (Parser.TRAININGDATA[i, classificationIdx] != prev)
                    return false;

            return true;
        }

        private IAttribute chooseAttribute(List<int> examples, List<IAttribute> attributes)
        {
            float tmp = -1;
            float max = -1;
            IAttribute maxAtr = null;

            foreach (IAttribute a in attributes)
            {

                if (a.IsOn)
                    tmp = a.CalculateGain(examples);

                if (tmp > max)
                {
                    max = tmp;
                    maxAtr = a;
                }
            }
            return maxAtr;
        }

        private INode majorityValue(List<int> attributes)
        {
            int n = 0;
            int p = 0;

            foreach(int i in attributes)
                if (Parser.TRAININGDATA[i, classificationIdx] == 1)
                    p++;
                else
                    n++;
            // leaf(1) leaf(1) funziona, 1 0 non va
            return p > n ? new Leaf(0) : new Leaf(1);
        }


        private List<int> CloneList(List<int> l)
        {
            List<int> aux = new List<int>();
            foreach (int i in l)
                aux.Add(i);

            return aux;
        }

    }
}
